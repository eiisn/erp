from django import forms
from erp.models import Condition, FieldValue, ObjectValue, BooleanValue, FloatValue, IntegerValue, StringValue, Line, \
    ASSOCIATION_TYPE

ASSOCIATION_COND = {
    'e': lambda v1, v2: v1 == v2,
    'l': lambda v1, v2: v1 < v2,
    'g': lambda v1, v2: v1 > v2,
    'le': lambda v1, v2: v1 <= v2,
    'ge': lambda v1, v2: v1 >= v2,
    'in': lambda v1, v2: v1 in v2,
}


def get_val(condition: Condition):
    cond = ASSOCIATION_TYPE[condition.val_2_type]
    if cond:
        return cond.objects.get(condition=condition)
    return None


def is_condition_pass(condition: Condition, val_2):
    return ASSOCIATION_COND[condition.cond](condition.val_1, val_2)
