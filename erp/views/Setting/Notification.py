from django.http import JsonResponse
from django.template import loader


def setting_notif(request):
    html = loader.render_to_string("erp/setting/notif/setting.html", {}, request)
    return JsonResponse({"html": html})
