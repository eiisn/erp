from django.http import JsonResponse
from django.template import loader


def setting_graph(request):
    html = loader.render_to_string("erp/setting/graph/setting.html", {}, request)
    return JsonResponse({"html": html})
