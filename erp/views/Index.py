from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render

from erp.models import Module, Notification, Chart


def index(request):
    charts = Chart.objects.all()
    all_module = Module.objects.all()
    side_link = {}
    for module in all_module:
        sub_module_link = {}
        all_sub_modules = module.submodule_set.filter(linked_module=None)
        for sub_module in all_sub_modules:
            if sub_module.field_set.count():
                sub_module_link[sub_module.name.replace(' ', '-')] = [
                    sub_module.name.capitalize(),
                    "erp/get-sub-module-index/%s/%s/" % (module.pk, sub_module.name),
                    sub_module.icon
                ]
        if sub_module_link:
            side_link[module.name.replace(' ', '-')] = {
                "name": module.name.capitalize(),
                "icon": module.icon,
                "sub_modules": sub_module_link
            }
    return render(request, 'index.html', {
        "sidebar": side_link,
        "notifications": Notification.objects.filter(closed=False),
        "charts": charts
    })


def close_notif(request, pk_notif):
    if request.is_ajax():
        notification = Notification.objects.get(pk=pk_notif)
        notification.closed = True
        notification.save()
        return JsonResponse({"data": "OK"})
    return HttpResponseForbidden()
