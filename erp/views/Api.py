from django.db.models import QuerySet
from rest_framework import status, authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_api_key.permissions import HasAPIKey
from erp.models import SubModule, Value, Line, ASSOCIATION_TYPE, ObjectValue, line_modified, line_totally_created


class APIViewERP(APIView):

    http_method_names = ['get', 'post', 'put', 'delete', 'head']
    authentication_classes = [authentication.SessionAuthentication, authentication.TokenAuthentication]
    permission_classes = [HasAPIKey | IsAuthenticated]

    def head(self, request, *args, **kwargs):
        return Response({"data": "OK", "module": kwargs.get('name', None)}, status=status.HTTP_200_OK)

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        pk_s = kwargs.get('pk_s', None)

        # Get SubModule
        sub_module, not_founded = self.get_sub_module(kwargs)
        if not_founded:
            return not_founded

        # Get fields
        fields = sub_module.field_set.all().order_by('order', 'name')

        # Check if is linked_module
        if sub_module.linked_module:
            pk_query = pk_s
            pk_sub_query = pk
        else:
            pk_query = pk
            pk_sub_query = pk_s

        if pk_query:
            try:
                line = sub_module.line_set.get(pk=pk_query)
            except Line.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)

            line_data = self.get_line_data(line, fields)
            sub_data = self.get_sub_lines(line)

            data = {**line_data, **sub_data}
        else:
            data = []
            if sub_module.linked_module:
                if not pk_sub_query:
                    return Response({"data": "Need pk of parent module Line"}, status=status.HTTP_403_FORBIDDEN)
                else:
                    lines = sub_module.line_set.filter(sub_module_line__pk=pk_sub_query)
            else:
                lines = sub_module.line_set.all()
            for line in lines:
                d = self.get_line_data(line, fields)
                sd = self.get_sub_lines(line)
                data.append({**d, **sd})
        return Response({"data": data})

    def put(self, request, *args, **kwargs):
        data = self.get_request_data(request)
        sub_module, not_founded = self.get_sub_module(kwargs)
        if not_founded:
            return not_founded
        line = Line.objects.get(pk=data.get('id'))
        fields = sub_module.field_set.all()
        new_data = {}
        return_data = {}
        modified = False

        # Get current value and set new value
        for field in fields:
            new_value = data.get(field.name, None)
            try:
                current_value = ASSOCIATION_TYPE[field.value_type].objects.get(line=line, field=field)
            except ASSOCIATION_TYPE[field.value_type].DoesNotExist:
                current_value = None

            if current_value and new_value is None and field.required:
                return Response({"data": "Field %s is required" % field.name}, status=status.HTTP_400_BAD_REQUEST)
            if field.value_type == 'obj':
                try:
                    new_value = Line.objects.get(pk=new_value)
                except Line.DoesNotExist:
                    if field.required:
                        return Response({"data": "Field %s is required" % field.name}, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        new_value = None
            elif field.value_type == 'int':
                new_value = int(new_value)
            elif field.value_type == 'float':
                new_value = float(new_value)
            new_data[field] = (current_value, new_value)

        # Check to delete value or save value change
        for field, (value, new_value) in new_data.items():
            if new_value is None:
                if value:
                    value.delete()
                return_data[field.name] = None
            else:
                if value:
                    if value.value != new_value:
                        value.value = new_value
                        value.save()
                        modified = True
                    return_data[field.name] = self.get_value(value)

        return Response({"data": return_data})

    def post(self, request, *args, **kwargs):
        data = self.get_request_data(request)
        pk = kwargs.get('pk', None)
        sub_module, not_founded = self.get_sub_module(kwargs)
        if not_founded:
            return not_founded

        new_line, new_data = self.new_line(sub_module, data)

        # Check if is linked_module
        if sub_module.linked_module:
            line = Line.objects.get(pk=pk)
            new_line.sub_module_line = line

        return_data = {}

        for key, value in data.items():
            try:
                sub_line_module = SubModule.objects.get(name=key)
                for d in value:
                    sub_new_line, sub_new_data = self.new_line(sub_line_module, d)
                    sub_new_line.sub_module_line = new_line
                    sub_new_line.save()
            except SubModule.DoesNotExist:
                continue

        new_line.save()
        return_data['id'] = new_line.pk

        for field, value in new_data.items():
            return_data[field.name] = self.get_value(value)
            value.save()
        line_totally_created.send(sender=Line, instance=new_line)
        return Response({"data": return_data})

    def new_line(self, sub_module, data):
        fields = sub_module.field_set.all()
        new_line = Line(sub_module=sub_module)

        new_data = {}
        for field in fields:
            value = data.pop(field.name.replace(' ', '-'), None)
            if value is None:
                value = data.pop(field.name, None)
            if value:
                if field.value_type == 'obj':
                    if isinstance(value, dict):
                        obj_sub_module = SubModule.objects.get(name=field.name)
                        value, value_data = self.new_line(obj_sub_module, value)
                    else:
                        value = Line.objects.get(pk=value)
                new_value = ASSOCIATION_TYPE[field.value_type](value=value, line=new_line, field=field)
            elif field.required:
                return Response({"data": "Field %s is required" % field.name}, status=status.HTTP_400_BAD_REQUEST)
            else:
                new_value = ASSOCIATION_TYPE[field.value_type](line=new_line, field=field)
            new_data[field] = new_value

        new_line.save()
        for field, value in new_data.items():
            value.save()

        return new_line, new_data

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        pk_s = kwargs.get('pk_s', None)
        sub_module, not_founded = self.get_sub_module(kwargs)
        if not_founded:
            return not_founded
        if sub_module.linked_module:
            pk_query = pk_s
        else:
            pk_query = pk
        line = Line.objects.get(pk=pk_query, sub_module=sub_module)
        line.delete()
        return Response()

    def get_request_data(self, request):
        data = {}
        for key, val in request.data.items():
            if val == "true":
                val = True
            elif val == "false":
                val = False
            elif val == '':
                val = None
            data[key.replace('-', ' ')] = val
        return data

    def get_line_data(self, line: Line, fields: QuerySet):
        data = {"id": line.pk}
        for field in fields:
            try:
                value = ASSOCIATION_TYPE[field.value_type].objects.get(line=line, field=field)
                data[field.name.replace(' ', '-')] = self.get_value(value)
            except ASSOCIATION_TYPE[field.value_type].DoesNotExist:
                data[field.name.replace(' ', '-')] = None
        return data

    def get_sub_lines(self, line):
        sub_data = {}
        sub_module_lines = Line.objects.filter(sub_module_line=line)

        for sub_line in sub_module_lines:
            sub_line_fields = sub_line.sub_module.field_set.all().order_by('order', 'name')
            if sub_line.sub_module.name not in sub_data:
                sub_data[sub_line.sub_module.name] = []
            sub_data[sub_line.sub_module.name].append(self.get_line_data(sub_line, sub_line_fields))
        return sub_data

    def get_value(self, val: Value):
        if isinstance(val, ObjectValue):
            if val.value:
                fields = val.value.sub_module.field_set.all()
                return self.get_line_data(val.value, fields)
            return None
        if isinstance(val.value, bool):
            return val.value
        return val.value

    def get_sub_module(self, params: dict):
        name = params.get('name', None)
        module_pk = params.get('module_pk', None)
        if not name:
            return None, Response({"data": "No module pk provided"}, status=status.HTTP_403_FORBIDDEN)
        if not name:
            return None, Response({"data": "No name provided"}, status=status.HTTP_403_FORBIDDEN)
        try:
            sub_module = SubModule.objects.get(name=name.replace('-', ' '), module__pk=module_pk.replace('-', ' '))
        except SubModule.DoesNotExist:
            msg = "Submodule %s doesn't exist" % name.replace('-', ' ')
            return None, Response({"data": msg}, status=status.HTTP_404_NOT_FOUND)
        return sub_module, None
