from django.db import models
from django.conf import settings

# Create your models here.


class Value(models.Model):
    value = None
    line = models.ForeignKey('Line', models.CASCADE, null=True)
    field = models.ForeignKey('Field', models.CASCADE, null=True)
    condition = models.ForeignKey('Condition', models.SET_NULL, null=True, blank=True)

    class Meta:
        abstract = True

    def is_condition_value(self):
        return True if self.condition else False

    def is_sub_module_value(self):
        return True if self.line and self.field else False

    def __str__(self):
        return str(self.value)


class BooleanValue(Value):
    value = models.BooleanField(default=False)


class StringValue(Value):
    value = models.CharField(max_length=1024, default="")


class IntegerValue(Value):
    value = models.IntegerField(default=0)


class FloatValue(Value):
    value = models.FloatField(default=0.00)


class DateValue(Value):
    value = models.DateField(null=True, blank=True)


class ObjectValue(Value):
    value = models.ForeignKey('erp.Line', models.SET_NULL, 'object_value', null=True)


class FieldValue(Value):
    value = models.ForeignKey('erp.Field', models.SET_NULL, 'field_value', null=True)


ASSOCIATION_TYPE = {
    'null': None,
    'str': StringValue,
    'int': IntegerValue,
    'float': FloatValue,
    'bool': BooleanValue,
    'obj': ObjectValue,
    'date': DateValue,
    'col': FieldValue,
}


class Module(models.Model):
    name = models.CharField(max_length=64, primary_key=True, unique=True)
    icon = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class SubModule(models.Model):
    name = models.CharField(max_length=64)
    icon = models.CharField(max_length=32)
    module = models.ForeignKey('Module', models.CASCADE, null=True)
    linked_module = models.ForeignKey('SubModule', models.SET_NULL, 'sub_module_linked', null=True, blank=True)

    def __str__(self):
        return "[%s] %s" % (self.module.name, self.name)


class Line(models.Model):
    sub_module = models.ForeignKey('SubModule', models.CASCADE, null=True)
    sub_module_line = models.ForeignKey('Line', models.CASCADE, 'sub_line', null=True, blank=True)

    def __str__(self):
        fields_display = self.sub_module.field_set.filter(display=True)
        values = []
        for field in fields_display:
            try:
                values.append(str(ASSOCIATION_TYPE[field.value_type].objects.get(line=self, field=field).value))
            except ASSOCIATION_TYPE[field.value_type].DoesNotExist:
                continue
        if values:
            return ", ".join(values)
        return "%s line %s" % (self.sub_module, self.id)


class Field(models.Model):
    name = models.CharField(max_length=64)
    sub_module = models.ForeignKey('SubModule', models.CASCADE, null=True)
    order = models.IntegerField(default=0)
    value_type = models.CharField(max_length=16, choices=settings.VAL_T, default='str')
    ref_sub_module_if_obj_value = models.ForeignKey('SubModule', models.CASCADE, 'sub_module_ref',
                                                    blank=True, null=True)
    start_end_or_none_if_date_value = models.BooleanField(choices=settings.PLANNING_START, default=None,
                                                          blank=True, null=True)
    required = models.BooleanField(default=False)
    display = models.BooleanField(default=False)

    def __str__(self):
        return "[%s][%s] %s (%s)" % (self.sub_module.module.name, self.sub_module.name, self.name, self.value_type)
