from django.db import models

CHART_TYPES = (
    ('line', 'Line'),
    ('bar', 'Bar'),
    ('radar', 'Radar'),
    ('doughnut', 'Donuts'),
    ('pie', 'Tarte'),
    ('polarArea', 'Polar'),
    ('bubble', 'Bubble'),
    ('scatter', 'Nuage de points'),
)


class Chart(models.Model):
    name = models.CharField(max_length=64)
    chart_type = models.CharField(max_length=24, choices=CHART_TYPES, default='line')

    def __str__(self):
        return self.name


class ChartLabel(models.Model):
    chart = models.ForeignKey('Chart', models.CASCADE)
    label = models.CharField(max_length=64)                 # Sale, Purchase, Invoice

    def __str__(self):
        return self.label


class ChartDataset(models.Model):
    chart = models.ForeignKey('Chart', models.CASCADE)
    label = models.CharField(max_length=64)                 # of sale, # of buy

    def __str__(self):
        return self.label


class DatasetValue(models.Model):
    label = models.ForeignKey('ChartLabel', models.CASCADE) # Sale, Purchase, Invoice
    dataset = models.ForeignKey('ChartDataset', models.CASCADE)
    value_field = models.ForeignKey('Field', models.CASCADE, blank=True, null=True)   # 0, 9, 12, 8
    value_sub_module = models.ForeignKey('SubModule', models.CASCADE, blank=True, null=True)

    def __str__(self):
        return "[%s][%s]" % (self.dataset, self.label)
