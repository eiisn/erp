from django.db import models
from django.conf import settings
# Create your models here.


class Event(models.Model):
    field = models.ForeignKey('Field', models.CASCADE)
    when = models.CharField(max_length=16, choices=settings.EVENTS, default='s')


class Step(models.Model):
    order = models.IntegerField()
    event = models.ForeignKey('Event', models.SET_NULL, null=True)
    # do = models.CharField(max_length=16, choices=settings.DO_EVT, default='c')


class StepDelete(models.Model):
    order = models.IntegerField()
    event = models.ForeignKey('Event', models.SET_NULL, null=True)


class Condition(models.Model):
    order = models.IntegerField()
    step = models.ForeignKey('Step', models.SET_NULL, null=True)
    statement = models.CharField(max_length=16, choices=settings.STATEMENT, default='if')
    val_1 = models.ForeignKey('Field', models.SET_NULL, 'val_1', null=True)
    cond = models.CharField(max_length=16, choices=settings.COND, default='e')
    val_2_type = models.CharField(max_length=16, choices=settings.VAL_T, default='null')
    c_else = models.ForeignKey('Condition', models.SET_NULL, null=True)


class Operator(models.Model):
    condition_1 = models.ForeignKey('Condition', models.SET_NULL, 'condition_1', null=True)
    condition_2 = models.ForeignKey('Condition', models.SET_NULL, 'condition_2', null=True)
    logic = models.CharField(max_length=8, choices=settings.LOGIC, default='or')
